package vn.util;

import com.google.gson.Gson;
import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.model.TextToSpeechData;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Utils {
    protected static final Logger logger = LogManager.getLogger("UtilsLog");

    public TextToSpeechData textToSpeech(String text) {
        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\r\n    \"input\":{\r\n      \"text\":\"" + text + "\"\r\n    },\r\n    \"voice\":{\r\n      \"languageCode\":\"en-US\",\r\n      \"name\":\"en-US-Standard-B\"\r\n    },\r\n    \"audioConfig\":{\r\n      \"audioEncoding\":\"MP3\"\r\n    }\r\n  }");
            Request request = new Request.Builder()
                    .url("https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=AIzaSyD3s8RKjGbJmZKgjM-vJnfJW0gg8QtlTFI")
                    .method("POST", body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            String res = response.body().string();
            Gson gson = new Gson();
            return gson.fromJson(res, TextToSpeechData.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static void synthesizeText(String text) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
        writer.write(text);

        writer.close();
    }
}
