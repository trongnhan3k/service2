package vn.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by nhanvt on 2020/06/09
 */
public abstract class BaseController extends Base {

    protected static final Logger logger = LoggerFactory.getLogger("RequestLog");

}
