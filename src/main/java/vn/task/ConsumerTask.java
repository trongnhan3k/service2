package vn.task;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import vn.base.BaseTask;
import vn.model.TextToSpeechData;
import vn.util.Utils;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by nhanvt on 2020/06/09
 */
public class ConsumerTask extends BaseTask implements Runnable {

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("192.168.66.125");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare("test", "fanout");
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, "test", "");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                //consume
                String message = new String(delivery.getBody(), "UTF-8");
                logger.info(" [x] Received message: " + message);

                Utils utils = new Utils();
                //text to speech
                TextToSpeechData textToSpeechData = utils.textToSpeech(message);

                //byte[] to file
                try {
                    Gson gson = new Gson();
                    Utils.synthesizeText(gson.toJson(textToSpeechData));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
            });
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
