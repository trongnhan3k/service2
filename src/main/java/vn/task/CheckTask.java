package vn.task;

import vn.base.BaseTask;
import vn.util.DateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * Created by nhanvt on 2020/06/09
 */
public class CheckTask extends BaseTask implements Runnable {
    DateUtils dateUtils = new DateUtils();

    @Override
    public void run() {
        while (true) {
            try {
                logger.info("--------Start check--------");
                Set<Thread> setThread = Thread.getAllStackTraces().keySet();
                List<String> lst = new ArrayList<>();

                setThread.forEach(x -> {
                    if (x.getName().startsWith("th_")) {
                        lst.add(x.getName());
                    }
                });

                Set<String> sorted = new TreeSet<>(lst);
                sorted.forEach(x -> {
                    logger.info("Thread [{}] is running", x);
                });

                logger.info("---------End check---------");

                TimeUnit.MINUTES.sleep(10);
            } catch (Exception e) {
                eLogger.error("error [{}]", e.getMessage());
            }
        }
    }
}
