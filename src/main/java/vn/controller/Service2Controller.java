package vn.controller;

import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vn.base.BaseController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by nhanvt on 2020/07/08
 */
@RestController
@RequestMapping(value = "/service2", method = RequestMethod.GET)
public class Service2Controller extends BaseController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String GETMedia() {
        return "Hello";
    }

    @GetMapping(value = "/getAudio")
    public ResponseEntity<StringBuilder> publish() throws IOException {
        logger.info("audio");
        BufferedReader br = new BufferedReader(new FileReader("output.txt"));
        StringBuilder resultStringBuilder = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            resultStringBuilder.append(line).append("\n");
        }
        br.close();
        Gson gson = new Gson();
        return new ResponseEntity<>(resultStringBuilder, HttpStatus.OK);
    }
}
