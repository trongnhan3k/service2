package vn;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import vn.base.Base;
import vn.task.CheckTask;
import vn.task.ConsumerTask;
import vn.util.Constant;

import java.lang.management.ManagementFactory;
import java.util.Properties;

/**
 * Created by nhanvt on 2020/06/09
 */
@SpringBootApplication
public class Service2Main extends Base {
    public static void main(String[] args) {
        Properties pros = new Properties();
        pros.put("server.port", Constant.SERVICE_PORT);

        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder()
                .sources(Service2Main.class)
                .properties(pros);
        applicationBuilder.run(args);

        logger.info("Service start at port {}\t{}", Constant.SERVICE_PORT, ManagementFactory.getRuntimeMXBean().getName());

        check();

        thread();
    }

    private static void thread() {
        ConsumerTask consumerTask = new ConsumerTask();
        Thread consumerThread = new Thread(consumerTask, "th_consumer");
        consumerThread.start();
    }

    private static void check() {
        CheckTask checkTask = new CheckTask();
        Thread checkThred = new Thread(checkTask, "th_check");
        checkThred.start();
    }
}

