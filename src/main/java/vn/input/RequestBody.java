package vn.input;

import lombok.Getter;

/**
 * Created by nhanvt on 2020/06/09
 */
@Getter
public class RequestBody {
    private String text;
}
