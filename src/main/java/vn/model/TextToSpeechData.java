package vn.model;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TextToSpeechData {
    private String audioContent;
}
